package com.xx.builddata.bean;

//产品信息
public class OrderInfo {
    private int channelOrderId; //产品ID
    private String channelName; //渠道（平台）名称
    private String cityName;    //城市
    private String productName;//产品名
    private String useTime;       //用车时间
    private String beginAddr;   //出发地点
    private String endAddr;     //终点
    private String carType;     //车型
    private String brandName;   //服务商名称
    private double estimatePrice;//价格
    private double estimateDistance;//距离
    private double estimateTimeLength;//时长

    public int getChannelOrderId() {
        return channelOrderId;
    }

    public void setChannelOrderId(int channelOrderId) {
        this.channelOrderId = channelOrderId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getBeginAddr() {
        return beginAddr;
    }

    public void setBeginAddr(String beginAddr) {
        this.beginAddr = beginAddr;
    }

    public String getEndAddr() {
        return endAddr;
    }

    public void setEndAddr(String endAddr) {
        this.endAddr = endAddr;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public double getEstimatePrice() {
        return estimatePrice;
    }

    public void setEstimatePrice(double estimatePrice) {
        this.estimatePrice = estimatePrice;
    }

    public double getEstimateDistance() {
        return estimateDistance;
    }

    public void setEstimateDistance(double estimateDistance) {
        this.estimateDistance = estimateDistance;
    }

    public double getEstimateTimeLength() {
        return estimateTimeLength;
    }

    public void setEstimateTimeLength(double estimateTimeLength) {
        this.estimateTimeLength = estimateTimeLength;
    }

    @Override
    public String toString() {
        return  channelOrderId + "\t" + channelName + "\t"
                + cityName + "\t" + productName + "\t" + useTime + "\t" + beginAddr
                + "\t" + endAddr + "\t" + carType + "\t" + brandName + "\t"
                + estimatePrice + "\t" + estimateDistance + "\t"
                + estimateTimeLength;
    }
}
