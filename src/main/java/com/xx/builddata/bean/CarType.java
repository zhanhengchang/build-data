package com.xx.builddata.bean;

import java.util.HashMap;
import java.util.Map;

public class CarType {
    private static Map<String, Object> JINGJI = new HashMap<>();
    private static Map<String, Object> SHUSHI = new HashMap<>();
    private static Map<String, Object> HAOHUA = new HashMap<>();
    private static Map<String, Object> SHANGWU = new HashMap<>();
    static {
        JINGJI.put("name", "经济型");
        JINGJI.put("rate_min", 90);
        JINGJI.put("rate_max", 110);

        SHUSHI.put("name", "舒适型");
        SHUSHI.put("rate_min", 110);
        SHUSHI.put("rate_max", 130);

        HAOHUA.put("name", "豪华型");
        HAOHUA.put("rate_min", 130);
        HAOHUA.put("rate_max", 140);

        SHANGWU.put("name", "商务型");
        SHANGWU.put("rate_min", 140);
        SHANGWU.put("rate_max", 150);
    }

    public static Map<String, Object> getJINGJI(){ return JINGJI; }

    public static Map<String, Object> getSHUSHI(){ return SHUSHI; }

    public static Map<String, Object> getHAOHUA(){
        return HAOHUA;
    }

    public static Map<String, Object> getSHANGWU(){
        return SHANGWU;
    }
}