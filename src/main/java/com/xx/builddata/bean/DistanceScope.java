package com.xx.builddata.bean;

import java.util.HashMap;
import java.util.Map;

public class DistanceScope {
    private static Map<Integer, Map<String, Integer>> map = new HashMap<>();
    public static Map<Integer, Map<String, Integer>> getMap() {
        Map<String, Integer> d5 = new HashMap<>();
        d5.put("min", 3);
        d5.put("max", 5);
        map.put(5, d5);

        Map<String, Integer> d10 = new HashMap<>();
        d10.put("min", 5);
        d10.put("max", 10);
        map.put(10, d10);

        Map<String, Integer> d15 = new HashMap<>();
        d15.put("min", 10);
        d15.put("max", 15);
        map.put(15, d15);

        Map<String, Integer> d20 = new HashMap<>();
        d20.put("min", 15);
        d20.put("max", 20);
        map.put(20, d20);

        Map<String, Integer> d30 = new HashMap<>();
        d30.put("min", 20);
        d30.put("max", 30);
        map.put(30, d30);

        Map<String, Integer> d40 = new HashMap<>();
        d40.put("min", 30);
        d40.put("max", 40);
        map.put(40, d40);

        Map<String, Integer> d50 = new HashMap<>();
        d50.put("min", 40);
        d50.put("max", 50);
        map.put(50, d50);
        return map;
    }
}