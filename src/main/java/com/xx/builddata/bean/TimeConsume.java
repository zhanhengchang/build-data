package com.xx.builddata.bean;

import java.util.HashMap;
import java.util.Map;

public class TimeConsume {
    private static Map<Integer, Map<String, Integer>> TIME = new HashMap<>();
    public static Map<Integer, Map<String, Integer>> getMap() {
        Map<String, Integer> d5 = new HashMap();
        d5.put("min", 8);
        d5.put("max", 10);
        TIME.put(5, d5);    //一定距离内需要消耗的最小时长、最大时长

        Map<String, Integer> d10 = new HashMap();
        d10.put("min", 12);
        d10.put("max", 16);
        TIME.put(10, d10);

        Map<String, Integer> d15 = new HashMap();
        d15.put("min", 18);
        d15.put("max", 22);
        TIME.put(15, d15);

        Map<String, Integer> d20 = new HashMap();
        d20.put("min", 25);
        d20.put("max", 35);
        TIME.put(20, d20);

        Map<String, Integer> d30 = new HashMap();
        d30.put("min", 40);
        d30.put("max", 50);
        TIME.put(30, d30);

        Map<String, Integer> d40 = new HashMap();
        d40.put("min", 70);
        d40.put("max", 80);
        TIME.put(40, d40);

        Map<String, Integer> d50 = new HashMap();
        d50.put("min", 72);
        d50.put("max", 85);
        TIME.put(50, d50);
        return TIME;
    }
}
