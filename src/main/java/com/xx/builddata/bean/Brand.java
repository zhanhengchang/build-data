package com.xx.builddata.bean;

import java.util.ArrayList;
import java.util.List;

public class Brand {
    public final static List<String> list = new ArrayList<>(16);
    static {
        list.add("900游");
        list.add("AA出行");
        list.add("三合出行");
        list.add("丽程");
        list.add("众至用车");
        list.add("呼我出行");
        list.add("天鹄专车");
        list.add("巴士专车");
        list.add("携程专车");
        list.add("携程专车特惠报价");
        list.add("携程专车返现");
        list.add("曹操专车");
        list.add("锦华股份");
        list.add("阳光出行");
        list.add("领优行");
        list.add("首汽约车");
    }
}
