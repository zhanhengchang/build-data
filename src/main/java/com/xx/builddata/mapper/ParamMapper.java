package com.xx.builddata.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;
import java.util.Map;

@Mapper
public interface ParamMapper {
    @Select("select * from ctrip_param_info")
    List<Map<String, Object>> getAllParams();
}
