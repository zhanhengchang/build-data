package com.xx.builddata.service;

import com.xx.builddata.bean.*;
import com.xx.builddata.mapper.ParamMapper;
import com.xx.builddata.util.LogUtil;
import com.xx.builddata.util.NumberUtil;
import com.xx.builddata.util.TimeUtil;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Service
public class LogService {
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;
    private final static String TOPIC = "ctrip";
    private final static String CHANNEL_NAME = "携程接送机";
    private final static String PRODUCT_NAME = "接送机";
    private final static int STANDARD = 25; //以每千米、经济型车辆的价格作为基准，预设为25元
    private final static int NUM = 5;      //每秒、每个线程产生的数据量

    public void buildData(Random random, List<Map<String, Object>> list) {
        Date date = new Date();
        System.out.println("开始发送数据到Kafka，时间：" + date);
        List<String> brandList = Brand.list;

//        PrintWriter printWriter = LogUtil.getPrintWriter();
//        LogUtil.saveLogs("这是脏数据", printWriter);
        kafkaTemplate.send(TOPIC, "这是脏数据");   //每一条参数模拟一条脏数据
        for(int i = 0; i<NUM; i++) {
            Map<String, Object> param = list.get(random.nextInt(list.size()));//每一条参数都有 4 种档次的车型
            try {
                //对于每一条参数，其起点、终点是确定的，要求距离不变，时长相同
                int distance = NumberUtil.getDistance(DistanceScope.getMap(), (Integer)param.get("distance"));
                int time = NumberUtil.getDistance(TimeConsume.getMap(), (Integer)param.get("distance"));
                String useTime = TimeUtil.getNow(date);
                brandList.forEach( brand -> {   //且假设每个品牌也都有这 4 种档次的车型
                    StringBuffer sb = new StringBuffer();
                    {
                        Map<String, Object> jingji = CarType.getJINGJI();
                        OrderInfo info = new OrderInfo();
                        info.setChannelName(CHANNEL_NAME);
                        info.setCityName((String) param.get("city_name"));
                        info.setProductName(PRODUCT_NAME);
                        info.setUseTime(useTime);//模拟生成产品，用车时间为当前时间
                        info.setBeginAddr((String) param.get("airport_name"));
                        info.setEndAddr((String) param.get("address"));
                        info.setCarType((String) jingji.get("name"));
                        info.setBrandName(brand);
                        //先根据表中的距离范围，获取实际生成的距离
                        info.setEstimateDistance(distance);
                        //计算随机价格
                        double rate = NumberUtil.getInteger((Integer) jingji.get("rate_min"), (Integer) jingji.get("rate_max")) * 1.0 / 100;
                        info.setEstimatePrice(NumberUtil.getFormat(distance * STANDARD * rate));//随机距离*基准*随机比率
                        info.setEstimateTimeLength(time);
//                        LogUtil.saveLogs(info.toString(), printWriter);
                        kafkaTemplate.send(TOPIC, info.toString());
                    }

                    {
                        Map<String, Object> shushi = CarType.getSHUSHI();
                        OrderInfo info = new OrderInfo();
                        info.setChannelName(CHANNEL_NAME);
                        info.setCityName((String) param.get("city_name"));
                        info.setProductName(PRODUCT_NAME);
                        info.setUseTime(useTime);
                        info.setBeginAddr((String) param.get("airport_name"));
                        info.setEndAddr((String) param.get("address"));
                        info.setCarType((String) shushi.get("name"));
                        info.setBrandName(brand);
                        info.setEstimateDistance(distance);
                        double rate = NumberUtil.getInteger((Integer) shushi.get("rate_min"), (Integer) shushi.get("rate_max")) * 1.0 / 100;
                        info.setEstimatePrice(NumberUtil.getFormat(distance * STANDARD * rate));
                        info.setEstimateTimeLength(time);
//                        LogUtil.saveLogs(info.toString(), printWriter);
                        kafkaTemplate.send(TOPIC, info.toString());
                    }

                    {
                        Map<String, Object> haohua = CarType.getHAOHUA();
                        OrderInfo info = new OrderInfo();
                        info.setChannelName(CHANNEL_NAME);
                        info.setCityName((String) param.get("city_name"));
                        info.setProductName(PRODUCT_NAME);
                        info.setUseTime(useTime);
                        info.setBeginAddr((String) param.get("airport_name"));
                        info.setEndAddr((String) param.get("address"));
                        info.setCarType((String) haohua.get("name"));
                        info.setBrandName(brand);
                        info.setEstimateDistance(distance);
                        double rate = NumberUtil.getInteger((Integer) haohua.get("rate_min"), (Integer) haohua.get("rate_max")) * 1.0 / 100;
                        info.setEstimatePrice(NumberUtil.getFormat(distance * STANDARD * rate));
                        info.setEstimateTimeLength(time);
//                        LogUtil.saveLogs(info.toString(), printWriter);
                        kafkaTemplate.send(TOPIC, info.toString());
                    }

                    {
                        Map<String, Object> shangwu = CarType.getSHANGWU();
                        OrderInfo info = new OrderInfo();
                        info.setChannelName(CHANNEL_NAME);
                        info.setCityName((String) param.get("city_name"));
                        info.setProductName(PRODUCT_NAME);
                        info.setUseTime(useTime);
                        info.setBeginAddr((String) param.get("airport_name"));
                        info.setEndAddr((String) param.get("address"));
                        info.setCarType((String) shangwu.get("name"));
                        info.setBrandName(brand);
                        info.setEstimateDistance(distance);
                        double rate = NumberUtil.getInteger((Integer) shangwu.get("rate_min"), (Integer) shangwu.get("rate_max")) * 1.0 / 100;
                        info.setEstimatePrice(NumberUtil.getFormat(distance * STANDARD * rate));
                        info.setEstimateTimeLength(time);
//                        LogUtil.saveLogs(info.toString(), printWriter);
                        kafkaTemplate.send(TOPIC, info.toString());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}