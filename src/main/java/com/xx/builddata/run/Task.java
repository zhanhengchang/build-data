package com.xx.builddata.run;

import com.xx.builddata.mapper.ParamMapper;
import com.xx.builddata.service.LogService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.*;

@Component
public class Task implements CommandLineRunner {//要执行的具体任务
    @Resource
    LogService logService;
    @Resource
    ParamMapper paramMapper;

    @Override
    public void run(String... args) {
        int threadNum = 3;
        List<Map<String, Object>> paramList = paramMapper.getAllParams();

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(threadNum);
        for (int i=0; i<threadNum; i++){
            scheduledExecutorService.scheduleAtFixedRate(() -> {
                Random random = new Random();
                logService.buildData(random, paramList);
            }, 0, 5, TimeUnit.SECONDS);
        }
    }
}
//每5秒966行，也即每分钟11592行。34个省的话大约每分钟每个省21个成交的。
//每5秒128kb，也即每分钟1.5M