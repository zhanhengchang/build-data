package com.xx.builddata.util;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Random;

public class NumberUtil {
    private static Random random = new Random();

    public static int getInteger(int min, int max){     //获取指定范围内的随机正整数
        return random.nextInt(max)%(max-min+1) + min;
    }

    public static int getDistance(Map<Integer, Map<String, Integer>> map, int n) throws Exception {//获取一个范围内的随机距离
        if (map.containsKey(n)){
            Map<String, Integer> scope = map.get(new Integer(n));
            int min = scope.get("min");
            int max = scope.get("max");
            return getInteger(min, max);
        }else {
            throw new Exception("非法距离参数：  distance = " + n);
        }
    }

    public static double getFormat(double x){
        BigDecimal b = new BigDecimal(x);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
}
