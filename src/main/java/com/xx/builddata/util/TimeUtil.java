package com.xx.builddata.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtil {
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static String[] times = {"00:00", "02:00", "04:00", "06:00", "08:00", "10:00", "12:00",
                                        "14:00", "16:00", "18:00", "20:00", "22:00"};

    public static String getTime(Date date, String time){
        return String.format("%s %s", sdf.format(date), time);
    }

    public static String[] getHours() {
        return times;
    }

    public static String getNow(Date date) {
        return sdf2.format(date);
    }
}