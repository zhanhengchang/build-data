package com.xx.builddata.util;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;

public class LogUtil {
    //测试时可以用这个类写入文件，替换Kafka模板send()方法
    private static HashMap<Long, PrintWriter> printWriterMap = new HashMap<>();

    public static void saveLogs(String message, PrintWriter printWriter){
        if (printWriter != null){
            printWriter.print(message);
            printWriter.print("\r\n");
            printWriter.flush();
        }
    }

    /**
     * 获取当前线程对应的PrintWriter
     * @return
     */
    public static PrintWriter getPrintWriter() {
        Thread thread = Thread.currentThread();
        if (printWriterMap.get(thread.getId()) != null){
            return printWriterMap.get(thread.getId());
        } else {
            try {
                FileWriter fileWriter = new FileWriter(String.format("e:/Ctrip-%s.txt", thread.getId()));
                PrintWriter printWriter = new PrintWriter(fileWriter);
                printWriterMap.put(thread.getId(), printWriter);
                return printWriter;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
}