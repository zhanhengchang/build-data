package com.xx.builddata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.xx.builddata.mapper")
@EnableCaching
@EnableScheduling
public class BuildDataApplication{

    public static void main(String[] args) {
        SpringApplication.run(BuildDataApplication.class, args);
    }
}